# Simple soft start

## Description
Simple soft start is used to limit inrush current when connecting big transformers or devices with big capacitors at input to mains voltage, like audio amplifiers, linear power supplies, etc..  
It uses resistors to limit current which are after set time bridged with relay to not waste power after connected device was sucesfully turned on.

## Visuals

## Parts selection
For your own safety use only parts from official distributors, do not use cheap e-waste parts from aliexpress, wish, etc..  

- **Current limiting resistors R1-R4**  
You will have to select value of these resistors depending on your application, but they should be wirewound flameproof resistors. In my application I'm using 4x27R so device connected to soft start can at startup draw max. (√2 * 230VAC) / (4 * 27R) = **~3Amps**.
- **Thermal fuse F1**  
Thermal fuse has to withstand current your device will draw and has to cut off the load in case soft start fails and current limiting resistors overheat. I recommend using 150 - 200˚C thermal fuse.
- **Relay RL1**  
Relay has to withstand currents drawing by your device connected to soft start. Most relays in this package will do 10-16Amps just fine.
- **Transistors**  
You can use most general purpose transistors with DC gain atleast 100.
- **Resistors R5, R6**  
They should be fusible or you will have to fuse the soft start externally. Every resistor should withstand atleast 500V to be resistant to high voltage surges or interference.
- **Capacitor C1**  
Has to be X2 rated film capacitor. 220n or 330n should be ok for 230V mains, for 110V mains you should double the capacitance.
- **Timing capacitor C8**  
This capacitor together with resistor R13 sets duration of soft starting. You should be careful with changing duration of soft start, the relay should only be switched on after the capacitor C7 is charged to enough voltage.

## PCB manufacturing & assembly
PCB is designed as single sided so you can easily etch or mill it at home. In generated gerber files is top layer, because most PCB fab houses charges same price for both one layer and two layer PCBs and also most PCB manufacturers won't apply solder mask on top layer if the PCB is single sided,but you can simply delete gerber file for top layer and have pcb fabricated as single sided.  
Assembly should be straight forward, all parts can be hand soldered, there are quite large clearences between SMD components and large solder pads make it easy to handsolder all components. Only problem can be soldering of the thermal fuse, you have to be fast to not overheat it.

## Usage
**Please be aware that the device is not isolated from mains, so only knowledgeable professionals should work with it. I'm giving you absolutely no warranty that this device will be safe and fully functional.**  
This circuity should be safely enclosured, so noone can get injured by touching it.

## Project status
Project is finished,but I might commit some small changes once in a while. If you want to make some changes feel free to fork this project.

## License
 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Vitix1/simple-soft-start">Simple soft start</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Vitix1">Vít Morávek (Vitix)</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 
